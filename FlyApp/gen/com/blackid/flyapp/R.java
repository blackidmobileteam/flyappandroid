/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.blackid.flyapp;

public final class R {
    public static final class attr {
    }
    public static final class drawable {
        public static final int account=0x7f020000;
        public static final int button_background=0x7f020001;
        public static final int error=0x7f020002;
        public static final int group=0x7f020003;
        public static final int home_icon=0x7f020004;
        public static final int ic_launcher=0x7f020005;
        public static final int icon=0x7f020006;
        public static final int plane=0x7f020007;
        public static final int splash=0x7f020008;
        public static final int tab_bg_selected=0x7f020009;
        public static final int tab_bg_selector=0x7f02000a;
        public static final int tab_bg_unselected=0x7f02000b;
        public static final int tile=0x7f02000c;
        public static final int title_backbround=0x7f02000d;
        public static final int weather1=0x7f02000e;
        public static final int weather_icon_cloud=0x7f02000f;
        public static final int weather_image_cloud_130=0x7f020010;
        public static final int yellow_warning_icon=0x7f020011;
    }
    public static final class id {
        public static final int LinearLayout01=0x7f060028;
        public static final int airport_code=0x7f060002;
        public static final int airport_full_name=0x7f060007;
        public static final int airport_list=0x7f06003a;
        public static final int airport_name=0x7f060001;
        public static final int backtoLogin=0x7f06002e;
        public static final int cpassword=0x7f060024;
        public static final int data_not_available=0x7f06001d;
        public static final int date=0x7f06000a;
        public static final int day=0x7f06000c;
        public static final int dropdown_txt=0x7f060036;
        public static final int emailId=0x7f060021;
        public static final int email_id=0x7f060040;
        public static final int first_name=0x7f06003e;
        public static final int firstname=0x7f06001f;
        public static final int forgot_password=0x7f06002b;
        public static final int imageView1=0x7f060003;
        public static final int imageView2=0x7f060012;
        public static final int indicator_img=0x7f06003b;
        public static final int indicator_txt=0x7f06003c;
        public static final int last_name=0x7f06003f;
        public static final int lastname=0x7f060020;
        public static final int login=0x7f06002a;
        public static final int logout=0x7f060035;
        public static final int nearest_airport_list=0x7f06002c;
        public static final int password=0x7f060023;
        public static final int profile_layout=0x7f06003d;
        public static final int profile_related_layout=0x7f060033;
        public static final int remember=0x7f060029;
        public static final int signup_btn=0x7f06002d;
        public static final int signup_fb=0x7f060031;
        public static final int signup_flyapp=0x7f060030;
        public static final int signup_layout=0x7f06002f;
        public static final int sky_condition=0x7f060013;
        public static final int spinner_txt=0x7f060037;
        public static final int splash_img=0x7f060000;
        public static final int state_country=0x7f060008;
        public static final int state_list=0x7f060039;
        public static final int state_name=0x7f060038;
        public static final int state_spinner=0x7f060025;
        public static final int temp_celcius=0x7f060016;
        public static final int temp_fahrenheit=0x7f060014;
        public static final int textView1=0x7f060004;
        public static final int textView10=0x7f06001e;
        public static final int textView2=0x7f060011;
        public static final int textView3=0x7f060017;
        public static final int textView4=0x7f060015;
        public static final int textView5=0x7f06001b;
        public static final int textView6=0x7f060009;
        public static final int textView7=0x7f060019;
        public static final int textView8=0x7f06001a;
        public static final int textView9=0x7f06000d;
        public static final int textView_registererror=0x7f060026;
        public static final int time=0x7f06000e;
        public static final int time_formate=0x7f06000f;
        public static final int update_profile=0x7f060027;
        public static final int user_login=0x7f060032;
        public static final int user_name=0x7f060041;
        public static final int username=0x7f060022;
        public static final int view1=0x7f060005;
        public static final int view2=0x7f060010;
        public static final int view_profile=0x7f060034;
        public static final int visibility=0x7f06001c;
        public static final int weather_detail=0x7f060006;
        public static final int wind_speed=0x7f060018;
        public static final int year=0x7f06000b;
    }
    public static final class layout {
        public static final int activity_splash=0x7f030000;
        public static final int airport_list_item=0x7f030001;
        public static final int airport_wether_layout=0x7f030002;
        public static final int edit_profile=0x7f030003;
        public static final int home_screen=0x7f030004;
        public static final int login_layout=0x7f030005;
        public static final int nearest_airport_list=0x7f030006;
        public static final int signup_layout=0x7f030007;
        public static final int signup_login_layout=0x7f030008;
        public static final int spinner_dropdown_layout=0x7f030009;
        public static final int spinner_txt_layout=0x7f03000a;
        public static final int state_list_item=0x7f03000b;
        public static final int state_list_screen=0x7f03000c;
        public static final int tab_indicator_layout=0x7f03000d;
        public static final int user_profile=0x7f03000e;
    }
    public static final class string {
        public static final int app_name=0x7f040000;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f050000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f050001;
    }
}
