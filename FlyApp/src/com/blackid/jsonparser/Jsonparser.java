package com.blackid.jsonparser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class Jsonparser {

	public int code;
	static InputStream is=null;
	String json="";
	
	static JSONObject jobject=null;
	
	public Jsonparser() {
		// TODO Auto-generated constructor stub
	}
	
	
	/**
	 * 
	 * @param url
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public String getJsonfromUrl(String url,String method) throws ClientProtocolException, IOException
	{
		
		StringBuilder builder=new StringBuilder();
		
		HttpParams params = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(params, 3500);
		HttpConnectionParams.setSoTimeout(params, 14000);
		HttpConnectionParams.setTcpNoDelay(params, false);
		
		params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
		params.setBooleanParameter(CoreProtocolPNames.USE_EXPECT_CONTINUE, true);   
		
		DefaultHttpClient httpClient=new DefaultHttpClient(params);
		HttpResponse httpResponse;
		
		if(method.equalsIgnoreCase("get")){
			HttpGet httpget	= new HttpGet(url);
			httpResponse=httpClient.execute(httpget);
			code=httpResponse.getStatusLine().getStatusCode();
			
			if(code==200)
			{
				HttpEntity httpentity=httpResponse.getEntity();
				is=httpentity.getContent();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
				
				String line=null;
				while((line=br.readLine())!=null)
				{
					builder.append(line+"\n");
				}
				is.close();
			}
			json=builder.toString();
		}else{
			HttpPost httppost=new HttpPost(url);
			httpResponse=httpClient.execute(httppost);
			code=httpResponse.getStatusLine().getStatusCode();
			
			if(code==200)
			{
				HttpEntity httpentity=httpResponse.getEntity();
				is=httpentity.getContent();
				
				BufferedReader br=new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
				
				String line=null;
				while((line=br.readLine())!=null)
				{
					builder.append(line+"\n");
				}
				is.close();
			}
			json=builder.toString();
		}
		
		return json;
		
	}
	
	/**
	 * 
	 * @param url
	 * @param params_name
	 * @return
	 */
	public String getJSONFromUrl(String url, List<NameValuePair> params_name) {

		// Making HTTP request
		try {
			// defaultHttpClient
			
			HttpParams params = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(params, 3500);
			HttpConnectionParams.setSoTimeout(params, 14000);
			HttpConnectionParams.setTcpNoDelay(params, false);
			
			params.setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
			params.setBooleanParameter(CoreProtocolPNames.USE_EXPECT_CONTINUE, true);   
			
			DefaultHttpClient httpClient = new DefaultHttpClient(params);
			HttpPost httpPost = new HttpPost(url);
			
			httpPost.setEntity(new UrlEncodedFormEntity(params_name,"UTF-8"));
			
			HttpResponse httpResponse = httpClient.execute(httpPost);
			
			code=httpResponse.getStatusLine().getStatusCode();
			
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			json = sb.toString();
			
			Log.e("STRING",json);
		
		} catch (Exception e) {
			Log.e("Buffer Error", "Error converting result " + e.toString());
		}
		
		Log.i("STRING",json);
		
		// try parse the string to a JSON object
		
		/*try {
			jobject = new JSONObject(json);			
		} catch (JSONException e) {
			Log.e("STRING",json);
			Log.e("JSON Parser", "Error parsing data yogesh" + e.toString());
		}*/

		//Log.i("STRING",json);
		// return JSON String
		return json;

	}
	
}
