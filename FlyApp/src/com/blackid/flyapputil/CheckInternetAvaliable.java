package com.blackid.flyapputil;


import com.blackid.flyapp.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnCancelListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckInternetAvaliable {
	
	ConnectivityManager connMgr;
	
	private Context _context;
	
	public boolean isInternetAvaliable;

	public CheckInternetAvaliable(Context context) {
		
		this._context = context;
		connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		
		isInternetAvaliable = ((connMgr.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED) || (connMgr.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED));
		
	}
	
	
	public void showSettingAlertDialog()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(_context);
		
		builder.setTitle("Network Connection Problem").setMessage("Unfortunately, The Network Connection is not available").setIcon(R.drawable.yellow_warning_icon);
		// Add the buttons
		builder.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		              
		        	  _context.startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
		        	  
		        	   //Toast.makeText(JsonDemoMainActivity.this, "COME BACK", Toast.LENGTH_LONG).show();
		        	   
		           }
		       });
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		           public void onClick(DialogInterface dialog, int id) {
		               // User cancelled the dialog
		        	   ((Activity)_context).finish();
		           }
		       });
		
		builder.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface arg0) {
				// TODO Auto-generated method stub
				
				((Activity)_context).finish();
				
			}
		});
		
		// Set other dialog properties

		// Create the AlertDialog
		AlertDialog dialog = builder.create();
		dialog.show();
		
	}
}
