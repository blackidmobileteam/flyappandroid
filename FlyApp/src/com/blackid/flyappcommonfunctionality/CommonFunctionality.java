package com.blackid.flyappcommonfunctionality;

import com.blackid.flyapp.HomeActivity;
import com.blackid.preferences.FlyAppPreferences;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;


public class CommonFunctionality {
	
	Context contextc;
	
	public CommonFunctionality(Context con)
	{
		contextc=con;
		
	}
	
	public void logOut()
	{
		SharedPreferences settings = FlyAppPreferences.getPreferences(contextc);
		SharedPreferences.Editor editor = settings.edit();
		editor.remove("login");
		editor.commit();
		
		final Intent intent = new Intent(contextc, HomeActivity.class);
		//final Intent intent = new Intent();
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		contextc.startActivity(intent);		
	}
}
