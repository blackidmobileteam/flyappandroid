package com.blackid.adapter;

import java.util.ArrayList;

import com.blackid.bean.Airport;
import com.blackid.flyapp.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AirportListAdapter extends BaseAdapter{
	
	ArrayList<Airport> airportData = new ArrayList<Airport>();
	Context context;
	LayoutInflater inflater;
	TextView airportName,airportCode;
	
	public AirportListAdapter(Context context,ArrayList<Airport> a) {
		// TODO Auto-generated constructor stub
		this.context	= context;
		airportData 	= a;
		inflater		= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return airportData.size();
	}

	@Override
	public Airport getItem(int position) {
		// TODO Auto-generated method stub
		return airportData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = inflater.inflate(R.layout.airport_list_item, parent, false);
		airportName	= (TextView)convertView.findViewById(R.id.airport_name);
		airportCode = (TextView)convertView.findViewById(R.id.airport_code);
		airportName.setText(airportData.get(position).getAirportName());
		airportCode.setText(airportData.get(position).getAirportCode());
		return convertView;
	}

}
