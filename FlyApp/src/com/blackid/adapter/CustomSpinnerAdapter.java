package com.blackid.adapter;

import java.util.ArrayList;
import java.util.List;

import com.blackid.flyapp.*;
import com.blackid.bean.*;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

public class CustomSpinnerAdapter extends ArrayAdapter<State>{
	
	ArrayList<State> objects;
	Context context;
	
	public CustomSpinnerAdapter(Context context, int textViewResourceId, ArrayList<State> objects) {
		super(context, textViewResourceId, objects);
		// TODO Auto-generated constructor stub
		this.objects = objects;
		this.context = context;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomDropDownView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		return getCustomView(position, convertView, parent);
	}
	
	public View getCustomView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(R.layout.spinner_txt_layout, parent, false);
        
        TextView label=(TextView)convertView.findViewById(R.id.spinner_txt);
        
        label.setText(objects.get(position).getStateName());

        return convertView;
    }
	
	public View getCustomDropDownView(int position, View convertView, ViewGroup parent){
		LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView=inflater.inflate(R.layout.spinner_dropdown_layout, parent, false);
        CheckedTextView t1 = (CheckedTextView)convertView.findViewById(R.id.dropdown_txt);
        t1.setText(objects.get(position).getStateName());
		return convertView;
	}
}
