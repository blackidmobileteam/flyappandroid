package com.blackid.adapter;

import java.util.ArrayList;

import com.blackid.bean.State;
import com.blackid.flyapp.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class StateListViewAdapter extends BaseAdapter{
	
	Context context;
	ArrayList<State> stateData;
	TextView stateName;
	LayoutInflater inflater;
	
	public StateListViewAdapter(Context context,ArrayList<State> stateData){
		this.context 	= context;
		this.stateData	= stateData;
		inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return stateData.size();
	}

	@Override
	public State getItem(int position) {
		// TODO Auto-generated method stub
		return stateData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = inflater.inflate(R.layout.state_list_item,parent,false);
		stateName = (TextView) convertView.findViewById(R.id.state_name);
		stateName.setText(stateData.get(position).getStateName());
		return convertView;
	}

}
