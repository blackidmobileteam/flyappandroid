package com.blackid.flyappdatabase;

import java.util.ArrayList;

import com.blackid.bean.Airport;
import com.blackid.bean.State;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class AirportContentProvider extends ContentProvider{
	
	
	
	public static final String SERVER_ID 				= "server_id";
	public static final String AIRPORT_NAME 			= "airport_name";
	public static final String AIRPORT_CODE 			= "airport_code";
	public static final String AIRPORT_STATE_CODE		= "state_code";
	
	static final String AIRPORT_TABLE_NAME 		= "airport_master";
	
    private SQLiteDatabase db;
    private MyHelper dbHelper;
    Context context;
    
    public AirportContentProvider(Context context) {
		// TODO Auto-generated constructor stub
    	this.context = context;
    	dbHelper = new MyHelper(this.context);
	}
    
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		db = dbHelper.getWritableDatabase();    
		int count = db.delete(AIRPORT_TABLE_NAME, selection, selectionArgs);  
		getContext().getContentResolver().notifyChange(uri, null);  
		Log.i("DELETE","DELETE AIRPORT RECORD");
		db.close();
		return count;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		db = dbHelper.getWritableDatabase();
		long rowID = db.insert(AIRPORT_TABLE_NAME, "", values);
		if (rowID > 0) {
			Uri _uri = ContentUris.withAppendedId(MyHelper.AIRPORT_CONTENT_URI, rowID);
            context.getContentResolver().notifyChange(_uri, null);    
        }
        Log.i("Test", "Data Inserted Successfully");
        db.close();
		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		Context context = getContext();
	    MyHelper dbHelper = new MyHelper(context);
	    db = dbHelper.getWritableDatabase();
	    db.close();
	      return (db == null)? false:true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		Cursor c = null;
		
		SQLiteQueryBuilder qb_airport = new SQLiteQueryBuilder();
		db = dbHelper.getReadableDatabase();
		qb_airport.setTables(AIRPORT_TABLE_NAME);
	    c = qb_airport.query(db, projection, selection, selectionArgs, null, sortOrder, sortOrder);
	    c.setNotificationUri(getContext().getContentResolver(), uri);
	    db.close();
	    return c;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		SQLiteDatabase db_deck = dbHelper.getWritableDatabase();    
		int count = db_deck.update(AIRPORT_TABLE_NAME, values, selection, selectionArgs);  
		getContext().getContentResolver().notifyChange(uri, null); 
		db.close();
		return count;
	}
	
	public boolean isEmptyAirportMaster() {
		
		db = dbHelper.getReadableDatabase();
	    
	    Cursor c = db.rawQuery("SELECT * FROM "+AIRPORT_TABLE_NAME, null);
	    
	    if(c.getCount()==0){
	    	return true;
	    }else{
	    	return false;
	    }
	}
	
	public ArrayList<Airport> getAirportsByStateCode(String state_code){
		
		db = dbHelper.getReadableDatabase();
	    ArrayList<Airport> airportList = new ArrayList<Airport>();
	    Cursor c = db.query(AIRPORT_TABLE_NAME, null, "state_code=?",new String[]{state_code}, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			Airport a = new Airport();
			a.setServerId(c.getInt(c.getColumnIndex(SERVER_ID)));
			a.setAirportName(c.getString(c.getColumnIndex(AIRPORT_NAME)));
			a.setAirportCode(c.getString(c.getColumnIndex(AIRPORT_CODE)));
			a.setStateCode(c.getString(c.getColumnIndex(AIRPORT_STATE_CODE)));
			airportList.add(a);
			c.moveToNext();
		}
		db.close();
		return airportList;
	}
}
