package com.blackid.flyappdatabase;

import android.content.Context;
import android.content.UriMatcher;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

public class MyHelper extends SQLiteOpenHelper{
	private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "FlyAppDB";
	
    static final String PROVIDER_NAME 			= "com.blackid.flyappdatabase";
	static final String AIRPORT_URL 			= "content://" + PROVIDER_NAME + "/airpots";
	public static final Uri AIRPORT_CONTENT_URI = Uri.parse(AIRPORT_URL);
	static final String STATE_URL 				= "content://" + PROVIDER_NAME + "/states";
	public static final Uri STATE_CONTENT_URI 	= Uri.parse(STATE_URL);
	static final String CITY_URL 				= "content://" + PROVIDER_NAME + "/cities";
	static final Uri CITY_CONTENT_URI 			= Uri.parse(CITY_URL);
	
	private static final int AIRPORT_URI_CODE 	= 1;
	private static final int STATE_URI_CODE 	= 2;
	private static final int CITY_URI_CODE 		= 3;
	private static UriMatcher uriMatcher 		= null;
    
    static {
    	
    	uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    	uriMatcher.addURI(PROVIDER_NAME	,"airporturi"	, AIRPORT_URI_CODE);
    	uriMatcher.addURI(PROVIDER_NAME	,"cityuri"		, STATE_URI_CODE);
    	uriMatcher.addURI(PROVIDER_NAME	,"cityuri"		, CITY_URI_CODE);
    }
    
    String CREATE_AIRPORTS_MASTER = "CREATE TABLE airport_master ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "server_id INTEGER , " +
            "airport_name TEXT, "+
            "airport_code TEXT, "+
            "state_code TEXT )";
	
	String CREATE_STATE_MASTER = "CREATE TABLE state_master ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "state_code TEXT, "+
            "state_name TEXT )";
	
	String CREATE_CITY_MASTER = "CREATE TABLE city_master ( " +
            "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "city_code TEXT, "+
            "city_name TEXT, "+
			"state_code TEXT )";
    
	public MyHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_AIRPORTS_MASTER);
		db.execSQL(CREATE_STATE_MASTER);
	//	db.execSQL(CREATE_CITY_MASTER);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		
	}
}
