package com.blackid.flyappdatabase;

import java.util.ArrayList;

import com.blackid.bean.State;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

public class StateContentProvider extends ContentProvider{
	
	public static final String STATE_NAME 	= "state_name";
	public static final String STATE_CODE 	= "state_code";
	
	public static final String STATE_TABLE_NAME	= "state_master";
	
	private SQLiteDatabase db;
    private MyHelper dbHelper;
    Context context;
    
    public StateContentProvider(Context context) {
		// TODO Auto-generated constructor stub
    	this.context = context;
    	dbHelper = new MyHelper(this.context);
	}
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		db = dbHelper.getWritableDatabase();    
		int count = db.delete(STATE_TABLE_NAME, selection, selectionArgs);  
		getContext().getContentResolver().notifyChange(uri, null);  
		Log.i("DELETE","DELETE STATE RECORD");
		db.close();
		return count;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		db = dbHelper.getWritableDatabase();
		long rowID = db.insert(STATE_TABLE_NAME, "", values);
		if (rowID > 0) {
			Uri _uri = ContentUris.withAppendedId(uri, rowID);
            context.getContentResolver().notifyChange(_uri, null);    
        }
        Log.i("Test", "Data Inserted Successfully");
        db.close();
		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		Context context = getContext();
	    MyHelper dbHelper = new MyHelper(context);
	    db = dbHelper.getWritableDatabase();
	    db.close();
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		Cursor c = null;
		
		SQLiteQueryBuilder qb_state = new SQLiteQueryBuilder();
		db = dbHelper.getReadableDatabase();
		qb_state.setTables(STATE_TABLE_NAME);
	    c = qb_state.query(db, null, selection, selectionArgs , null, null, null);
	    c.setNotificationUri(getContext().getContentResolver(), uri);
	    db.close();
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		SQLiteDatabase db_deck = dbHelper.getWritableDatabase();    
		int count = db_deck.update(STATE_TABLE_NAME, values, selection, selectionArgs);  
		getContext().getContentResolver().notifyChange(uri, null); 
		db.close();
		return count;
	}
	
	public boolean isEmptyStateMaster() {
		
		db = dbHelper.getReadableDatabase();
	    
	    Cursor c = db.rawQuery("SELECT * FROM "+STATE_TABLE_NAME, null);
	    
	    if(c.getCount()==0){
	    	return true;
	    }else{
	    	return false;
	    }
	}
	
	public ArrayList<State> getAllStates(){
		
		db = dbHelper.getReadableDatabase();
	    ArrayList<State> stateList = new ArrayList<State>();
	    Cursor c = db.query(STATE_TABLE_NAME, null, null, null, null, null, null);
		c.moveToFirst();
		while (!c.isAfterLast()) {
			State s = new State();
			s.setStateCode(c.getString(c.getColumnIndex(STATE_CODE)));
			s.setStateName(c.getString(c.getColumnIndex(STATE_NAME)));
			stateList.add(s);
			c.moveToNext();
		}
		db.close();
		return stateList;
	}
	
	public State getStateByStateCode(String stateCode){
		
		db = dbHelper.getReadableDatabase();
	    
	    Cursor c = db.query(STATE_TABLE_NAME, null,"state_code=?",new String[]{stateCode}, null, null, null);
		c.moveToFirst();
		State s = new State();
		s.setStateCode(c.getString(c.getColumnIndex(STATE_CODE)));
		s.setStateName(c.getString(c.getColumnIndex(STATE_NAME)));
		
		db.close();
		return s;
	}
	
}
