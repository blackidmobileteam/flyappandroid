package com.blackid.flyapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.bean.State;
import com.blackid.flyapp.StateListActivity.AsyncAirportList;
import com.blackid.flyappdatabase.StateContentProvider;
import com.blackid.flyapputil.CheckInternetAvaliable;
import com.blackid.jsonparser.Jsonparser;
import com.blackid.preferences.FlyAppPreferences;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class UpdateProfile extends Activity{
	
	TextView fname,lname,userName,emailId,stateName;
	String str_fname,str_lname,str_username,str_emailid,str_state;
	LinearLayout profile;
	ProgressDialog dialog;
	Button editProfile;
	String userId;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_profile);
		
		String is_user_loggedin = FlyAppPreferences.readString(UpdateProfile.this, "login", "");
		
		if(!is_user_loggedin.equalsIgnoreCase("logged")){
			Intent i = new Intent(UpdateProfile.this,LoginActivity.class);
			startActivity(i);
			UpdateProfile.this.finish();
		}
		

		fname 		= (TextView)findViewById(R.id.first_name);
		lname 		= (TextView)findViewById(R.id.last_name);
		userName	= (TextView)findViewById(R.id.user_name);
		emailId		= (TextView)findViewById(R.id.email_id);
		stateName	= (TextView)findViewById(R.id.state_name);
		profile		= (LinearLayout)findViewById(R.id.profile_layout);
		editProfile	= (Button)findViewById(R.id.update_profile);
		
		dialog		= new ProgressDialog(this);
		dialog.setMessage("Loading...");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		
		if(!(new CheckInternetAvaliable(UpdateProfile.this).isInternetAvaliable)){
			CheckInternetAvaliable network=new CheckInternetAvaliable(UpdateProfile.this);
			network.showSettingAlertDialog();
		}else{
			new AsyncProfileData(UpdateProfile.this).execute();
		}
		
		editProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(UpdateProfile.this,EditProfileActivity.class);
				i.putExtra("fname"		, fname.getText());
				i.putExtra("lname"		, lname.getText());
				i.putExtra("username"	, userName.getText());
				i.putExtra("emailid"	, emailId.getText());
				i.putExtra("state"		, str_state);
				startActivity(i);
			}
		});
	}

	class AsyncProfileData extends AsyncTask<Void, Void, Void>{
		
		Context context;
		JSONObject json_object;
		String str_status="",msg;
		Jsonparser json_parser;
		String json;
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		public AsyncProfileData(Context con) {
			// TODO Auto-generated constructor stub
			context = con;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			json_parser = new Jsonparser();
			dialog.show();
		}
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			try {
				userId	= FlyAppPreferences.readString(UpdateProfile.this, "user_id", "");
				Log.e("userid",userId);
				String url = FlyAppConstant.LOCAL_URL+"users/view/"+userId+"/device";
				
				Log.e("url",url);
				
				json = json_parser.getJsonfromUrl(FlyAppConstant.SERVER_URL+"users/view/"+userId+"/device","post");
				
				Log.e("json data",json);
				json_object = new JSONObject(json);
				
				str_fname 		= json_object.getString("firstname");
				str_lname		= json_object.getString("lastname");
				str_username	= json_object.getString("username");
				str_emailid		= json_object.getString("email_id");
				str_state		= json_object.getString("state");
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			StateContentProvider scp = new StateContentProvider(UpdateProfile.this);
			State s1 = scp.getStateByStateCode(str_state);
			fname.setText(str_fname);
			lname.setText(str_lname);
			userName.setText(str_username);
			emailId.setText(str_emailid);
			stateName.setText(s1.getStateName());
			profile.setVisibility(View.VISIBLE);
			dialog.dismiss();
		}
	}
	
	
}
