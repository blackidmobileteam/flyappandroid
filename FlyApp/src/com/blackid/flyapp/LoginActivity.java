package com.blackid.flyapp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.jsonparser.Jsonparser;
import com.blackid.preferences.FlyAppPreferences;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity{
	
	EditText username,password;
	CheckBox rememberMe;
	String str_username,str_password;
	Drawable erroricon;
	ProgressDialog dialog;
	Button loginBtn,forgotPassword;
	
	private static final String TAG_STATUS="status";
	private static final String TAG_MESSAGE="message";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login_layout);
		
		username 		= (EditText)findViewById(R.id.username);
		password 		= (EditText)findViewById(R.id.password);
		rememberMe		= (CheckBox)findViewById(R.id.remember);
		
		loginBtn		= (Button)findViewById(R.id.login);
		forgotPassword	= (Button)findViewById(R.id.forgot_password);
		
		dialog		= new ProgressDialog(this);
		dialog.setMessage("Loading...");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		
		erroricon=getResources().getDrawable(R.drawable.error);
		erroricon.setBounds(new Rect(0, 0, erroricon.getIntrinsicWidth(), erroricon.getIntrinsicHeight()));
		
		loginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getValueOfAll();
				if((!isEmpty(str_username)) && (!isEmpty(str_password))){
					new AsyncLogin(LoginActivity.this).execute();
				}else{
					Toast.makeText(LoginActivity.this, "Some Field Missing", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
	
	private void getValueOfAll()
	{
		str_username=validateEditText(username,"User Name");		
		str_password=validateEditText(password,"Password");
	}
	
	private String validateEditText(EditText e,String value)
	{
		if(e.getText().toString().equalsIgnoreCase(""))
		{
			/*e.setHint(value);
			e.setHintTextColor(Color.RED);
			e.requestFocus();*/
			e.setError(value, erroricon);
			return "";
		}
		else
		{
			return e.getText().toString();
		}
	}
	public Boolean isEmpty(String s)
	{
		if(s.equalsIgnoreCase(""))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	class AsyncLogin extends AsyncTask<Void, Void, Void>{
		
		Context context;
		JSONObject json_object;
		String str_status="",msg;
		Jsonparser json_parser;
		String json;
		String userId;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		public AsyncLogin(Context con) {
			// TODO Auto-generated constructor stub
			context = con;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			json_parser = new Jsonparser();
			dialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... param) {
			// TODO Auto-generated method stub
			params.add(new BasicNameValuePair("username"		, str_username));
			params.add(new BasicNameValuePair("password"		, str_password));
			
			json = json_parser.getJSONFromUrl(FlyAppConstant.SERVER_URL+"users/login/device", params);
			
			try {
				json_object = new JSONObject(json);
				str_status  = json_object.getString(TAG_STATUS);
				msg			= json_object.getString(TAG_MESSAGE);
				userId		= json_object.getString("id");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(str_status.equalsIgnoreCase("error")){
				Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
			}else{
				//Toast.makeText(LoginActivity.this, msg, Toast.LENGTH_LONG).show();
				dialog.dismiss();
				
				FlyAppPreferences.writeString(LoginActivity.this, "login", "logged");
				FlyAppPreferences.writeString(LoginActivity.this, "user_id", userId);
				Log.e("userid",userId);
				Intent i = new Intent(LoginActivity.this,UpdateProfile.class);
				startActivity(i);
				finish();
			}
			
		}

	}
}
