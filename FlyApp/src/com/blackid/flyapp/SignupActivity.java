package com.blackid.flyapp;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.adapter.CustomSpinnerAdapter;
import com.blackid.bean.State;
import com.blackid.flyappdatabase.StateContentProvider;
import com.blackid.jsonparser.Jsonparser;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SignupActivity extends Activity{

	EditText fname,lname,username,password,cpassword,emailid;
	String str_fname,str_lname,str_username,str_password,str_cpassword,str_emailid,str_state;
	Drawable erroricon;
	Spinner stateList;
	Button signupBtn,backloginBtn;
	TextView textError;
	ProgressDialog dialog;
	ArrayList<State> stateData = new ArrayList<State>();
	
	private static final String TAG_STATUS="status";
	private static final String TAG_MESSAGE="message";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup_layout);
		
		fname		= (EditText)findViewById(R.id.firstname);
		lname		= (EditText)findViewById(R.id.lastname);
		emailid		= (EditText)findViewById(R.id.emailId);
		username	= (EditText)findViewById(R.id.username);
		password	= (EditText)findViewById(R.id.password);
		cpassword	= (EditText)findViewById(R.id.cpassword);
		textError	= (TextView)findViewById(R.id.textView_registererror);
		signupBtn	= (Button)findViewById(R.id.signup_btn);
		backloginBtn= (Button)findViewById(R.id.backtoLogin);
		stateList	= (Spinner)findViewById(R.id.state_spinner);
		
		dialog		= new ProgressDialog(this);
		dialog.setMessage("Loading...");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		erroricon=getResources().getDrawable(R.drawable.error);
		erroricon.setBounds(new Rect(0, 0, erroricon.getIntrinsicWidth(), erroricon.getIntrinsicHeight()));
		
		StateContentProvider scp = new StateContentProvider(SignupActivity.this);
		
		stateData = scp.getAllStates();
		if(stateData.size()==0){
			Log.e("state data","state data is blanck");
		}
		CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter(SignupActivity.this, R.layout.spinner_txt_layout, stateData);
		stateList.setAdapter(spinnerAdapter);
		
		emailid.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				Boolean valid_email=isEmailValid(emailid.getText().toString());
				if(valid_email==false)
				{
					emailid.setError("Invalid Email", erroricon);
				}
			}
		});
		
		backloginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignupActivity.this,LoginActivity.class);
				startActivity(i);
				
			}
		});
		
		signupBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getValueOfAll();
				if(isEmpty(str_username) && isEmpty(str_password) && isEmpty(str_cpassword) && isEmpty(str_fname) && 
						   isEmpty(str_lname)  && isEmpty(str_emailid) && isEmpty(str_state))
						{
							if(!(str_password.equals(str_cpassword)))
							{
								cpassword.setError("Password Missmatch",erroricon );
							}
							else
							{
								textError.setVisibility(View.GONE);
								new AsyncSignup(SignupActivity.this).execute();
							}
						}
						else
						{
							Toast.makeText(SignupActivity.this, "Some Field Missing", Toast.LENGTH_LONG).show();
						}
			}
		});
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	}
	
	private void getValueOfAll()
	{
		str_username=validateEditText(username,"User Name");		
		str_password=validateEditText(password,"Password");
		str_cpassword=validateEditText(cpassword,"Confirm Password");
		str_fname=validateEditText(fname,"First Name");
		str_lname=validateEditText(lname,"Last Name");
		str_emailid=validateEditText(emailid,"Email id");
		str_state = stateData.get(stateList.getSelectedItemPosition()).getStateCode();
	}
	
	
	private String validateEditText(EditText e,String value)
	{
		if(e.getText().toString().equalsIgnoreCase(""))
		{
			/*e.setHint(value);
			e.setHintTextColor(Color.RED);
			e.requestFocus();*/
			e.setError(value, erroricon);
			return "";
		}
		else
		{
			return e.getText().toString();
		}
	}
	
	public boolean isEmailValid(String email)
    {
		String not_valid="";
		String[] test = email.split(",");
		//Toast.makeText(contextc, test.length+"", Toast.LENGTH_LONG).show();
		String regExpn =
       		 "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
		 
		for(int i=0;i<test.length;i++)
		{
			CharSequence inputStr = test[i];
			Matcher matcher = pattern.matcher(inputStr);
		    if(matcher.matches())
		    {
		    	//return true;
		    }
		    else
		    {
		    	if(i==0 || i==test.length-1)
		    	{
		    		not_valid=not_valid+(i+1);
		    	}
		    	else
		    	{
		    		not_valid=not_valid+(i+1)+",";
		    	}
		    }
		}
		if(not_valid.equalsIgnoreCase(""))
		{
			return true;
		}
		else
		{
			return false;
		}
		
    }
	
	public Boolean isEmpty(String s)
	{
		if(s.equalsIgnoreCase(""))
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	class AsyncSignup extends AsyncTask<Void, Void, Void>{
		
		Context context;
		JSONObject json_object;
		String str_status="",msg;
		Jsonparser json_parser;
		String json;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		public AsyncSignup(Context context) {
			// TODO Auto-generated constructor stub
			this.context = context;
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			json_parser=new Jsonparser();
			dialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... param) {
			// TODO Auto-generated method stub
			params.add(new BasicNameValuePair("firstname"		, str_fname));
			params.add(new BasicNameValuePair("lastname"		, str_lname));
			params.add(new BasicNameValuePair("username"		, str_username));
			params.add(new BasicNameValuePair("email_id"		, str_emailid));
			params.add(new BasicNameValuePair("password"		, str_password));
			params.add(new BasicNameValuePair("confirm_password", str_cpassword));
			params.add(new BasicNameValuePair("state"			, str_state));
			
			json = json_parser.getJSONFromUrl(FlyAppConstant.SERVER_URL+"users/signup", params);
			
			try {
				json_object = new JSONObject(json);
				str_status  = json_object.getString(TAG_STATUS);
				msg			= json_object.getString(TAG_MESSAGE);
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(str_status.equalsIgnoreCase("error")){
				textError.setVisibility(View.VISIBLE);
				textError.setText(msg);
			}else{
				Toast.makeText(SignupActivity.this, msg, Toast.LENGTH_LONG).show();
				Intent i = new Intent(SignupActivity.this,LoginActivity.class);
				startActivity(i);
				SignupActivity.this.finish();
			}
			dialog.dismiss();
		}

	}
}
