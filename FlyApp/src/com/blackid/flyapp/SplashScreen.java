package com.blackid.flyapp;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.flyapp.StateListActivity.AsyncAirportList;
import com.blackid.flyappdatabase.*;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.flyappdatabase.AirportContentProvider;
import com.blackid.flyapputil.CheckInternetAvaliable;
import com.blackid.jsonparser.Jsonparser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

public class SplashScreen extends Activity{
	ImageView splashImg;
	int SPLASH_DISPLAY_LENGTH = 3000;
	StateContentProvider scp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);
		
		splashImg = (ImageView)findViewById(R.id.splash_img);
		splashImg.setScaleType(ScaleType.FIT_XY);
		
		scp = new StateContentProvider(SplashScreen.this);
		boolean staterecords 	  = scp.isEmptyStateMaster();
        
        Log.e("state info",""+staterecords);
        
        if(staterecords){
	    	if(!(new CheckInternetAvaliable(SplashScreen.this).isInternetAvaliable)){
				CheckInternetAvaliable network=new CheckInternetAvaliable(SplashScreen.this);
				network.showSettingAlertDialog();
			}else{
				new AsyncStateList(SplashScreen.this).execute();
			}
        }	
        
		//
		new Handler().postDelayed(new Runnable() {

			public void run() {
				
				SplashScreen.this.finish();
				Intent mainIntent = new Intent(SplashScreen.this, HomeActivity.class);
				SplashScreen.this.startActivity(mainIntent);
			}
                
        }, SPLASH_DISPLAY_LENGTH);
        
	}
	
class AsyncStateList extends AsyncTask<Void, Void, Void>{
		
		Context context;
		JSONArray jsonStateData;
		Jsonparser jsonParser;
		String jsonStateList;
		String TAG_STATE_NAME = "name";
		String TAG_STATE_CODE = "code";
		
		public AsyncStateList(Context con) {
			// TODO Auto-generated constructor stub
			context = con;
			jsonParser = new Jsonparser();
			
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String url = FlyAppConstant.SERVER_URL+"states";
			Log.i("State download url",url);
			try {
				jsonStateList = jsonParser.getJsonfromUrl(url,"post");
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			try {
				jsonStateData = new JSONArray(jsonStateList);
				for(int i=0;i<jsonStateData.length();i++){
					JSONObject data 	= jsonStateData.getJSONObject(i);
					
					ContentValues values 		= new ContentValues();
					values.put(StateContentProvider.STATE_CODE	, data.getString(TAG_STATE_CODE));
					values.put(StateContentProvider.STATE_NAME	, data.getString(TAG_STATE_NAME));
					scp.insert(MyHelper.STATE_CONTENT_URI, values);
				}
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
			if(new CheckInternetAvaliable(SplashScreen.this).isInternetAvaliable) {
				
			}else {
				
				AlertDialog.Builder alert_dialog_builder = new AlertDialog.Builder(SplashScreen.this);
				alert_dialog_builder.setMessage("No Network Available").setCancelable(true).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
	
					public void onClick(DialogInterface arg0, int arg1) {
						finish();
						onDestroy();
					}
				});
	
				AlertDialog alert = alert_dialog_builder.create();
				alert.setTitle("No Network");
				alert.show();
			}
			
			
		}
	}
}
	