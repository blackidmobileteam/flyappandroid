package com.blackid.flyapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.opengl.Visibility;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class HomeActivity extends Activity{
	
	LayoutInflater inflater;
	ImageView indicatorImage;
	TextView indicatorText;
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen);
		final TabHost tabHost = (TabHost)findViewById(android.R.id.tabhost);
		LocalActivityManager mLocalActivityManager = new LocalActivityManager(this, false);
	    mLocalActivityManager.dispatchCreate(savedInstanceState);
	    tabHost.setup(mLocalActivityManager);
	    
	    inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    
        TabSpec tab1 = tabHost.newTabSpec("First Tab");
        TabSpec tab2 = tabHost.newTabSpec("Second Tab");
        TabSpec tab3 = tabHost.newTabSpec("Third tab");
        TabSpec tab4 = tabHost.newTabSpec("Forth tab");

       // Set the Tab name and Activity
       // that will be opened when particular Tab will be selected
        tab1.setIndicator("Tab1");
        
		View view1 = inflater.inflate(R.layout.tab_indicator_layout, null,false);
        
		indicatorImage = (ImageView)view1.findViewById(R.id.indicator_img);
		indicatorText  = (TextView)view1.findViewById(R.id.indicator_txt);
		indicatorText.setVisibility(View.GONE);
		
        tab1.setIndicator(view1);
        tab1.setContent(new Intent(this,NearestAirportActivity.class));
        
        View view2 = inflater.inflate(R.layout.tab_indicator_layout, null,false);
		indicatorImage = (ImageView)view2.findViewById(R.id.indicator_img);
		LinearLayout.LayoutParams vp = 
		        new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, 
		                        LayoutParams.WRAP_CONTENT);
		indicatorImage.setLayoutParams(vp);
		
		//indicatorImage.setLayoutParams(new LayoutParams(20,20));
		indicatorImage.setImageResource(R.drawable.plane);
		indicatorText  = (TextView)view2.findViewById(R.id.indicator_txt);
        indicatorText.setText("Airports");
        tab2.setIndicator(view2);
        tab2.setContent(new Intent(this,StateListActivity.class));
        
        View view3 = inflater.inflate(R.layout.tab_indicator_layout, null,false);
        indicatorImage = (ImageView)view3.findViewById(R.id.indicator_img);
        indicatorImage.setLayoutParams(vp);
        indicatorImage.setImageResource(R.drawable.group);
		indicatorText  = (TextView)view3.findViewById(R.id.indicator_txt);
		indicatorText.setText("Meet Up");
		
        tab3.setIndicator(view3);
        tab3.setContent(new Intent(this,Tab3Activity.class));
        
        View view4 = inflater.inflate(R.layout.tab_indicator_layout, null,false);
        indicatorImage = (ImageView)view4.findViewById(R.id.indicator_img);
        indicatorImage.setLayoutParams(vp);
        indicatorImage.setImageResource(R.drawable.account);
		indicatorText  = (TextView)view4.findViewById(R.id.indicator_txt);
		indicatorText.setText("Account");
		
        tab4.setIndicator(view4);
        tab4.setContent(new Intent(this,SignupLoginActivity.class));
        
        /** Add the tabs  to the TabHost to display. */
        tabHost.addTab(tab1);
        tabHost.addTab(tab2);
        tabHost.addTab(tab3);
        tabHost.addTab(tab4);
        tabHost.setCurrentTab(0);
	}

}
