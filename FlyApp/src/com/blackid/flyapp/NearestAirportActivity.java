package com.blackid.flyapp;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.adapter.AirportListAdapter;
import com.blackid.bean.Airport;
import com.blackid.flyapputil.CheckInternetAvaliable;
import com.blackid.gpstracker.GPSTracker;
import com.blackid.jsonparser.Jsonparser;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class NearestAirportActivity extends Activity{
	ListView nearestAirportList;
	ArrayList<Airport> airportList = new ArrayList<Airport>();
	AirportListAdapter airportListAdapter;
	double latitude;
	double longitude;
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nearest_airport_list);
        nearestAirportList = (ListView)findViewById(R.id.nearest_airport_list);
        
        GPSTracker gps = new GPSTracker(NearestAirportActivity.this);
        
        if(gps.canGetLocation()){
        	latitude	= gps.getLatitude();
        	longitude	= gps.getLongitude();
        	
        	if(!(new CheckInternetAvaliable(NearestAirportActivity.this).isInternetAvaliable)){
    			CheckInternetAvaliable network=new CheckInternetAvaliable(NearestAirportActivity.this);
    			network.showSettingAlertDialog();
    		}else{
    			new AsyncGetNearAirport(NearestAirportActivity.this).execute();
    		}
        	
    	}else{
        	showDialog();
        }
        
    }
	
	public void showDialog(){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NearestAirportActivity.this);
 
			// set title
			alertDialogBuilder.setTitle("GPS Tracker Alert");
 
			// set dialog message
			alertDialogBuilder
				.setMessage("Please On GPS Tracker to fetch nearest airports")
				.setCancelable(false)
				.setPositiveButton("ok",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.dismiss();
					}
				  });
 
			AlertDialog alertDialog = alertDialogBuilder.create();
 
			alertDialog.show();
	}
	
	class AsyncGetNearAirport extends AsyncTask<Void, Void, Void>{
		
		Context context;
		ProgressDialog dialog;
		String json;
		JSONObject jsonairportList;
		JSONArray nearestAirpotData;
		String AIRPORT_CODE = "fs";
		String AIRPORT_NAME = "name";
		
		public AsyncGetNearAirport(Context context) {
			// TODO Auto-generated constructor stub
			this.context = context;
			dialog = new ProgressDialog(context);
			this.dialog.setMessage("Fetching Nearest Airpots");
			this.dialog.setCancelable(true);
			this.dialog.setCanceledOnTouchOutside(false);
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			this.dialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String url = FlyAppConstant.FLY_APP_URL +"airports/rest/v1/json/withinRadius/"+longitude+"/"+latitude+"/150?appId="+FlyAppConstant.appId+"&appKey="+FlyAppConstant.appKey;
			Log.e("json home url",url);
			Jsonparser parser = new Jsonparser();
			try {
				json	= parser.getJsonfromUrl(url,"get");
				
				try {
					jsonairportList = new JSONObject(json);
					nearestAirpotData = jsonairportList.getJSONArray("airports");
					
					if(nearestAirpotData.length()>0){
						for(int i=0;i<nearestAirpotData.length();i++){
							JSONObject airport = nearestAirpotData.getJSONObject(i);
							Airport a1 = new Airport();
							a1.setAirportCode(airport.getString(AIRPORT_CODE));
							a1.setAirportName(airport.getString(AIRPORT_NAME));
							airportList.add(a1);
						}
					}
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Log.e("json data",json);
			
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			this.dialog.dismiss();
			airportListAdapter = new AirportListAdapter(NearestAirportActivity.this, airportList);
			nearestAirportList.setAdapter(airportListAdapter);
			nearestAirportList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					Intent i = new Intent(NearestAirportActivity.this,AirportWeatherReport.class);
					i.putExtra("airport_code", airportList.get(position).getAirportCode());
					startActivity(i);
				}
			});
		}

	}
}
 