package com.blackid.flyapp;

import com.blackid.flyappcommonfunctionality.CommonFunctionality;
import com.blackid.preferences.FlyAppPreferences;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.LocalActivityManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class SignupLoginActivity extends Activity{
	
	Button signupBtn,loginBtn,signupfbBtn,viewProfile,logout;
	LinearLayout l1,l2;
	boolean visibilityStatus=false;
	CommonFunctionality cf;
	@Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_login_layout);
        
        signupBtn 	= (Button)findViewById(R.id.signup_flyapp);
        signupfbBtn = (Button)findViewById(R.id.signup_fb);
        loginBtn 	= (Button)findViewById(R.id.user_login);
        l1			= (LinearLayout)findViewById(R.id.signup_layout);
        l2			= (LinearLayout)findViewById(R.id.profile_related_layout);
        viewProfile	= (Button)findViewById(R.id.view_profile);
        logout		= (Button)findViewById(R.id.logout);
        
        cf = new CommonFunctionality(SignupLoginActivity.this);
        
        String is_user_loggedin = FlyAppPreferences.readString(SignupLoginActivity.this, "login", "");
		
		if(is_user_loggedin.equalsIgnoreCase("logged")){
			l1.setVisibility(View.GONE);
			l2.setVisibility(View.VISIBLE);
		}
        
        signupBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignupLoginActivity.this,SignupActivity.class);
				startActivity(i);
			}
			
		});
        
        loginBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignupLoginActivity.this,LoginActivity.class);
				startActivity(i);
			}
		});
        
        viewProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(SignupLoginActivity.this,UpdateProfile.class);
				startActivity(i);
			}
		});
        
        logout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				cf.logOut();
			}
		});
           
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		//super.onActivityResult(requestCode, resultCode, data);
		
	}
	
}
