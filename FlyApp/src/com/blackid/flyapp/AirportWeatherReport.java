package com.blackid.flyapp;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.flyapputil.CheckInternetAvaliable;
import com.blackid.jsonparser.Jsonparser;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AirportWeatherReport extends Activity{
	
	LinearLayout weatherDetail,dataNotAvailable;
	TextView dateTxt,yearTxt,timeTxt,dayTxt;
	TextView airportName,stateCountry;
	TextView tempFahrenhit,tempCelcius;
	TextView skyCondition,windSpeed,visibility;
	String airport_code;
	boolean dataAvailable;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.airport_wether_layout);
		
		Intent i = getIntent();
		airport_code = i.getStringExtra("airport_code");
		
		weatherDetail		= (LinearLayout)findViewById(R.id.weather_detail);
		dataNotAvailable	= (LinearLayout)findViewById(R.id.data_not_available);
		dateTxt 			= (TextView)findViewById(R.id.date);
		yearTxt				= (TextView)findViewById(R.id.year);
		timeTxt				= (TextView)findViewById(R.id.time);
		airportName			= (TextView)findViewById(R.id.airport_full_name);
		stateCountry		= (TextView)findViewById(R.id.state_country);
		tempFahrenhit		= (TextView)findViewById(R.id.temp_fahrenheit);
		tempCelcius			= (TextView)findViewById(R.id.temp_celcius);
		skyCondition		= (TextView)findViewById(R.id.sky_condition);
		windSpeed			= (TextView)findViewById(R.id.wind_speed);
		visibility			= (TextView)findViewById(R.id.visibility);
		dayTxt				= (TextView)findViewById(R.id.day);
		
		if(!(new CheckInternetAvaliable(AirportWeatherReport.this).isInternetAvaliable)){
			CheckInternetAvaliable network=new CheckInternetAvaliable(AirportWeatherReport.this);
			network.showSettingAlertDialog();
		}else{
			new AsyncFetchWeatherInfo(AirportWeatherReport.this).execute();
		}
		
		//Toast.makeText(AirportWeatherReport.this, "Airport code:"+airport_code, Toast.LENGTH_LONG).show();
	}
	
	class AsyncFetchWeatherInfo extends AsyncTask<Void, Void, Void>{
		
		Context context;
		Jsonparser jsonParser;
		String json;
		JSONObject jsonWeatherData;
		String airport_name,state_country,wind_speed,visibilityStr,sky_condition,time,temp_celcius,temp_fahrenhit,day;
		String[] dateElement;
		ProgressDialog dialog = new ProgressDialog(AirportWeatherReport.this);
		boolean dataAvailable = false;
		
		public AsyncFetchWeatherInfo(Context context) {
			// TODO Auto-generated constructor stub
			this.context = context;
			jsonParser = new Jsonparser();
			this.dialog.setMessage("Fetching Weather Detail");
			this.dialog.setCancelable(true);
			this.dialog.setCanceledOnTouchOutside(false);
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			this.dialog.show();
		}
		
		@SuppressLint("SimpleDateFormat")
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			String url = FlyAppConstant.FLY_APP_URL+"weather/rest/v1/json/metar/"+airport_code+"?appId="+FlyAppConstant.appId+"&appKey="+FlyAppConstant.appKey;
			Log.e("url",url);
			
			try {
				json = jsonParser.getJsonfromUrl(url,"get");
				
				Log.e("json data",json);
				jsonWeatherData = new JSONObject(json);
				
				JSONArray jsonAirportData = jsonWeatherData.getJSONObject("appendix").getJSONArray("airports");
				
				if(jsonAirportData.length()>0){
					dataAvailable		   = true;
					JSONObject airportData = jsonAirportData.getJSONObject(0);
					JSONObject weatherData = jsonWeatherData.getJSONObject("metar");
					JSONObject weatherConditions = weatherData.getJSONObject("conditions");
					
					airport_name = airportData.getString("name");
					
					Log.e("airport name",airport_name);
					
					
					state_country = airportData.getString("city")+" ,"+airportData.getString("countryName");
					
					Log.e("state name",state_country);
					wind_speed 		= weatherConditions.getJSONObject("wind").getString("speedKnots");
					visibilityStr	= weatherConditions.getJSONObject("visibility").getString("miles");
					sky_condition	= weatherConditions.getJSONArray("skyConditions").getJSONObject(0).getString("coverage");
					temp_celcius		= weatherData.getString("temperatureCelsius");
					
					Log.e("airport name",wind_speed);
					Log.e("airport name",visibilityStr);
					Log.e("airport name",sky_condition);
					Log.e("airport name",temp_celcius+"°C");
					
					float fahrenhit = (float) (Float.parseFloat(temp_celcius)+33.8);
					
					temp_fahrenhit = ""+fahrenhit+"°";
					
					String dateStr	= airportData.getString("localTime");
					String date = dateStr.substring(0, 10);
					
					dateElement = date.split("-");
					time = dateStr.substring(11,16);
					
					Calendar c = new GregorianCalendar(Integer.parseInt(dateElement[0]),Integer.parseInt(dateElement[1]),Integer.parseInt(dateElement[2]));
					Date d= (new GregorianCalendar(Integer.parseInt(dateElement[0]),Integer.parseInt(dateElement[1]),Integer.parseInt(dateElement[2]))).getTime();
					SimpleDateFormat f = new SimpleDateFormat("EEE");
				    day=f.format(d);
					Log.e("day",""+day);
					
					Log.e("date str",""+dateElement[0]+"/"+dateElement[1]);
					Log.e("airport name",""+time);
					Log.e("airport name",""+dateElement[2]);
					
				}
					
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(dataAvailable){
				weatherDetail.setVisibility(View.VISIBLE);
				airportName.setText(airport_name);
				stateCountry.setText(state_country);
				windSpeed.setText(wind_speed);
				visibility.setText(visibilityStr);
				skyCondition.setText(sky_condition);
				tempCelcius.setText(temp_celcius+"°C");
				tempFahrenhit.setText(temp_fahrenhit);
				dateTxt.setText(""+dateElement[2]+"/"+dateElement[1]);
				timeTxt.setText(""+time);
				yearTxt.setText(""+""+dateElement[0]);
				dayTxt.setText(day);
				
			}else{
				dataNotAvailable.setVisibility(View.VISIBLE);
			}
			this.dialog.dismiss();
		}

	}
}
