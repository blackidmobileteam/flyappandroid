package com.blackid.flyapp;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.Constants.FlyAppConstant;
import com.blackid.adapter.AirportListAdapter;
import com.blackid.adapter.StateListViewAdapter;
import com.blackid.bean.Airport;
import com.blackid.bean.State;
import com.blackid.flyappdatabase.AirportContentProvider;
import com.blackid.flyappdatabase.MyHelper;
import com.blackid.flyappdatabase.StateContentProvider;
import com.blackid.flyapputil.CheckInternetAvaliable;
import com.blackid.jsonparser.Jsonparser;
import com.blackid.preferences.FlyAppPreferences;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class StateListActivity  extends Activity
{
	ListView stateList,airportList;
	ProgressDialog dialog;
	StateContentProvider scp;
	AirportContentProvider acp;
	ArrayList<State> stateListArr;
	StateListViewAdapter stateListAdapter;
	ArrayList<Airport> airportListArr = new ArrayList<Airport>();
	LinearLayout layout;
	boolean backtobutton = false;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.state_list_screen);
        
        stateList 	= (ListView)findViewById(R.id.state_list);
        airportList = (ListView)findViewById(R.id.airport_list);
        
        scp = new StateContentProvider(StateListActivity.this);
        acp	= new AirportContentProvider(StateListActivity.this);
        
        boolean airportRecords	  = acp.isEmptyAirportMaster();
        
        Log.e("state info",""+airportRecords);
        
        if(airportRecords){
        	if(!(new CheckInternetAvaliable(StateListActivity.this).isInternetAvaliable)){
    			CheckInternetAvaliable network=new CheckInternetAvaliable(StateListActivity.this);
    			network.showSettingAlertDialog();
    		}else{
    			new AsyncAirportList().execute();
    		}
        }else{
        	displayStateList();
        }
        
    }
    
    @Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
    	if(backtobutton==true){
    		super.onBackPressed();
    	}else{
    		airportList.setVisibility(View.GONE);
    		stateList.setVisibility(View.VISIBLE);
    		backtobutton = true;
    	}
    	
	}

	public void displayStateList(){
    	stateListArr = new ArrayList<State>();
    	stateListArr = scp.getAllStates();
    	stateListAdapter = new StateListViewAdapter(StateListActivity.this, stateListArr);
    	stateList.setAdapter(stateListAdapter);
    	
    	stateList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				State s1 = stateListArr.get(position);
				
				AirportContentProvider acp = new AirportContentProvider(StateListActivity.this);
				airportListArr = acp.getAirportsByStateCode(s1.getStateCode());
				
				AirportListAdapter airportListAdapter = new AirportListAdapter(StateListActivity.this, airportListArr);
				airportList.setAdapter(airportListAdapter);
				airportList.setVisibility(View.VISIBLE);
				stateList.setVisibility(View.GONE);
			}
		});
    	
    	airportList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				// TODO Auto-generated method stub
				Intent i = new Intent(StateListActivity.this,AirportWeatherReport.class);
				i.putExtra("airport_code", airportListArr.get(position).getAirportCode());
				startActivity(i);
			}
		});
    }
	
	class AsyncAirportList extends AsyncTask<Void, Void, Void>{
		
		Context context;
		Jsonparser jsonPrser = new Jsonparser();
		JSONArray jsonData;
		String jsonairportList;
		
		String TAG_AIRPORT 				= "Airport";
		String TAG_STATE 				= "State";
		String TAG_AIRPORT_NAME 		= "name";
		String TAG_AIRPORT_CODE 		= "flight_stats_code";
		String TAG_AIRPORT_SERVER_ID	= "id";
		String TAG_STATE_CODE			= "code";
		
		
		ProgressDialog dialog 			= new ProgressDialog(StateListActivity.this);
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			this.dialog.setMessage("Downloading resourses");

			this.dialog.setCancelable(true);

			this.dialog.setCanceledOnTouchOutside(false);
			this.dialog.show();
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String url = FlyAppConstant.SERVER_URL+"airports/airportListByCountry/US";
			Log.i("Server hit string",url);
			try {
				jsonairportList = jsonPrser.getJsonfromUrl(FlyAppConstant.SERVER_URL+"airports/airportListByCountry/US","post");
			} catch (ClientProtocolException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				jsonData = new JSONArray(jsonairportList);
				for(int i=0;i<jsonData.length();i++){
					JSONObject data 	= jsonData.getJSONObject(i);
					JSONObject airport 	= data.getJSONObject(TAG_AIRPORT);
					JSONObject state	= data.getJSONObject(TAG_STATE);
					
					
					ContentValues values 		= new ContentValues();
					values.put(AirportContentProvider.SERVER_ID			, airport.getString(TAG_AIRPORT_SERVER_ID));
					values.put(AirportContentProvider.AIRPORT_NAME		, airport.getString(TAG_AIRPORT_NAME));
					values.put(AirportContentProvider.AIRPORT_CODE		, airport.getString(TAG_AIRPORT_CODE));
					values.put(AirportContentProvider.AIRPORT_STATE_CODE, state.getString(TAG_STATE_CODE));
					acp.insert(MyHelper.AIRPORT_CONTENT_URI, values);
					
					
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			this.dialog.dismiss();
			displayStateList();
		}
		
	}

}

