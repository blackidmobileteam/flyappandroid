package com.blackid.bean;

public class Airport {

	public int server_id;
	public String airportCode;
	public String airportName;
	public String stateCode;
	
	public Airport(){
		
	}
	
	public Airport(int server_id,String airportCode,String airportName,String stateCode){
		this.server_id = server_id;
		this.airportCode = airportCode;
		this.airportName = airportName;
		this.stateCode = stateCode;
	}
	
	public void setServerId(int server_id){
		this.server_id = server_id;
	}
	
	public void setAirportCode(String airportCode){
		this.airportCode = airportCode;
	}
	
	public void setAirportName(String airportName){
		this.airportName = airportName;
	}
	
	public void setStateCode(String stateCode){
		this.stateCode = stateCode;
	}
	
	public int getServerId(){
		return server_id;
	}
	
	public String getAirportCode(){
		return airportCode;
	}
	
	public String getAirportName(){
		return airportName;
	}
	
	public String getStateCode(){
		return stateCode;
	}
	
}
