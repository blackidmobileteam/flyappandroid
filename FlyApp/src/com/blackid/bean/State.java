package com.blackid.bean;

public class State {
	public String stateName;
	public String stateCode;
	
	public State(){
		
	}
	
	public State(String state_name,String state_code){
		stateName = state_name;
		stateCode = state_code;
	}
	
	public void setStateName(String state_name){
		stateName = state_name;
	}
	
	public void setStateCode(String state_code){
		stateCode = state_code;
	}
	
	public String getStateName(){
		return stateName;
	}
	
	public String getStateCode(){
		return stateCode;
	}
	
}
