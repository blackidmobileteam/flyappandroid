package com.blackid.Constants;

public class FlyAppConstant {
	
	public static final String FLY_APP_URL	= "https://api.flightstats.com/flex/";
	public static final String SERVER_URL	= "http://demo.blackidlabs.com/fly_app/";
	public static final String LOCAL_URL	= "http://192.168.1.17/flying_app_services/";
	
	public static final String appId		 = "17fddf0e";
	public static final String appKey		 = "a1309f4d7918a38a58c9665c142a9557";
	
}
